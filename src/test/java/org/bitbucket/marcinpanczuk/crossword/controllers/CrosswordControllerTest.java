package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.bitbucket.marcinpanczuk.crossword.controllers.utils.CheckboxResultContainer;
import org.bitbucket.marcinpanczuk.crossword.controllers.utils.Pattern;
import org.bitbucket.marcinpanczuk.crossword.controllers.utils.PatternListContainer;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.bitbucket.marcinpanczuk.crossword.services.WordManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.ModelAndViewAssert.assertAndReturnModelAttributeOfType;
import static org.springframework.test.web.ModelAndViewAssert.assertViewName;


class CrosswordControllerTest {

    @InjectMocks
    private CrosswordController crosswordController;
    @Mock
    private WordManager wordManager;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Nested
    @DisplayName("patternLengthsForm() ModelAndView")
    class patternLengthsForm {

        @Test
        void should_Be_NotBull() {
            ModelAndView mav = crosswordController.patternsLengthsForm();

            assertNotNull(mav);
        }

        @Test
        void should_ViewName_BeSet_Crossword() {
            ModelAndView mav = crosswordController.patternsLengthsForm();

            assertViewName(mav, "crossword");
        }

        @Test
        void should_CheckboxResult_BeType_CheckboxResultContainer() {
            ModelAndView mav = crosswordController.patternsLengthsForm();

            assertAndReturnModelAttributeOfType(mav, "checkboxResult", CheckboxResultContainer.class);
        }
    }

    @Nested
    @DisplayName("patternsForm() ModelAndView")
    class patternsForm {

        @Test
        void should_Be_NotNull() {
            CheckboxResultContainer checkboxResultContainer = Mockito.mock(CheckboxResultContainer.class);
            BindingResult bindingResult = Mockito.mock(BindingResult.class);
            ModelAndView mav = crosswordController.patternsForm(checkboxResultContainer, bindingResult);

            assertNotNull(mav);
        }

        @Test
        void should_PatternFormObject_BeType_PatternListContainer() {
            CheckboxResultContainer checkboxResultContainer = Mockito.mock(CheckboxResultContainer.class);
            BindingResult bindingResult = Mockito.mock(BindingResult.class);
            ModelAndView mav = crosswordController.patternsForm(checkboxResultContainer, bindingResult);

            assertAndReturnModelAttributeOfType(mav, "patternForm", PatternListContainer.class);
        }

        @Test
        void should_PatternFormObject_DependOn_CheckboxResult() {
            List<String> checkboxResult = new ArrayList<>();
            checkboxResult.add("4");
            checkboxResult.add("5");
            CheckboxResultContainer checkboxResultContainer = new CheckboxResultContainer();
            checkboxResultContainer.setList(checkboxResult);

            Pattern pattern1 = new Pattern();
            pattern1.setPatternLength("4");
            Pattern pattern2 = new Pattern();
            pattern2.setPatternLength("5");
            List<Pattern> patternsList = new ArrayList<>();
            patternsList.add(pattern1);
            patternsList.add(pattern2);
            PatternListContainer expected = new PatternListContainer();
            expected.setPatternList(patternsList);

            BindingResult bindingResult = Mockito.mock(BindingResult.class);

            ModelAndView mav = crosswordController.patternsForm(checkboxResultContainer, bindingResult);
            PatternListContainer actual = (PatternListContainer) mav.getModelMap().getAttribute("patternForm");

            assertEquals(expected.getPatternList().size(), actual.getPatternList().size());
            assertSame(expected.getPatternList().get(0).getPatternLength(),
                    actual.getPatternList().get(0).getPatternLength());
            assertSame(expected.getPatternList().get(1).getPatternLength(),
                    actual.getPatternList().get(1).getPatternLength());
        }
    }

    @Nested
    @DisplayName("searchResult() ModelAndView")
    class searchResult {

        @Test
        void should_Be_NotNull(){
            PatternListContainer patternListContainer = Mockito.mock(PatternListContainer.class);
            BindingResult bindingResult = Mockito.mock(BindingResult.class);

            ModelAndView mav = crosswordController.searchResult(patternListContainer, bindingResult);

            assertNotNull(mav);
        }

        @SuppressWarnings("unchecked")
        @Test
        void should_CreateSummarizedResults_For_PatternsInPatternListContainer() {
            Pattern pattern1 = new Pattern();
            pattern1.setPattern("___bł_");
            Pattern pattern2 = new Pattern();
            pattern2.setPattern("_rzw_");
            List<Pattern> patternList = new ArrayList<>();
            patternList.add(pattern1);
            patternList.add(pattern2);
            PatternListContainer patternListContainer = new PatternListContainer();
            patternListContainer.setPatternList(patternList);

            List<Word> resultsForPattern1 = new ArrayList<>();
            resultsForPattern1.add(new Word("źdźbło"));
            List<Word> resultsForPattern2 = new ArrayList<>();
            resultsForPattern2.add(new Word("drzwi"));
            List<List<Word>> expected = new ArrayList<>();
            expected.add(resultsForPattern1);
            expected.add(resultsForPattern2);

            BindingResult bindingResult = Mockito.mock(BindingResult.class);

            Mockito.when(wordManager.getWordByPattern("___bł_")).thenReturn(resultsForPattern1);
            Mockito.when(wordManager.getWordByPattern("_rzw_")).thenReturn(resultsForPattern2);

            ModelAndView mav = crosswordController.searchResult(patternListContainer, bindingResult);
            List<List<Word>> actual = (List<List<Word>>) mav.getModelMap().getAttribute("summarizedResults");

            assertEquals(expected.size(), actual.size());
            assertEquals(expected.get(0).size(), actual.get(0).size());
            assertSame(expected.get(0).get(0), actual.get(0).get(0));
        }
    }
}