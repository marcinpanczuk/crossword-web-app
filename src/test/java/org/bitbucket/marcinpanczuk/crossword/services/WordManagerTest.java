package org.bitbucket.marcinpanczuk.crossword.services;

import org.bitbucket.marcinpanczuk.crossword.dao.WordDAO;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WordManagerTest {

    @InjectMocks
    private WordManager wordManager;
    @Mock
    private WordDAO wordDAO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void should_ReturnTrue_When_AddWordReturnTrue() {
        String wordId = "źdźbło";
        Word word = new Word(wordId);
        Mockito.when(wordDAO.add(word)).thenReturn(true);

        boolean result = wordManager.addWord(wordId);

        assertTrue(result);
    }

    @Test
    void should_ReturnFalse_When_AddWordReturnFalse() {
        String wordId = "źdźbło";
        Word word = new Word(wordId);
        Mockito.when(wordDAO.add(word)).thenReturn(false);

        boolean result = wordManager.addWord(wordId);

        assertFalse(result);
    }

    @Test
    void should_ReturnWord_When_GetWordReturnWord() {
        String wordId = "źdźbło";
        Word actual = new Word(wordId);
        Word expected = new Word("źdźbło");
        Mockito.when(wordDAO.get(wordId)).thenReturn(actual);

        Word result = wordManager.getWord(wordId);

        assertEquals(expected, actual);
    }

    @Test
    void should_ReturnNull_When_GetWordReturnNull() {
        String wordId = "źdźbło";
        Mockito.when(wordDAO.get(wordId)).thenReturn(null);

        Word result = wordManager.getWord(wordId);

        assertNull(result);
    }

    @Test
    void should_ReturnList_When_ListReturnedByWordDAO() {
        String pattern = "___bł_";
        Word word = new Word("źdźbło");
        List<Word> list = new ArrayList<>();
        list.add(word);
        List<Word> expected = new ArrayList<>();
        expected.add(word);
        Mockito.when(wordDAO.getByPattern(pattern)).thenReturn(list);

        List<Word> actual = wordManager.getWordByPattern(pattern);

        assertEquals(expected.size(), actual.size());
        assertEquals(expected, actual);
    }

    @Test
    void should_ReturnNull_When_NullReturnedByWordDAO() {
        String pattern = "___bł_";
        Mockito.when(wordDAO.getByPattern(pattern)).thenReturn(null);

        List<Word> actual = wordManager.getWordByPattern(pattern);

        assertNull(actual);
    }

    @Test
    void should_ReturnWordsList_Which_DontContainExcludedChars(){
        String excludedChars = "bd";
        List<Word> words = new ArrayList<>();
        words.add(new Word("baca"));
        words.add(new Word("gard"));
        words.add(new Word("kali"));
        words.add(new Word("mans"));
        List<Word> expected = new ArrayList<>();
        expected.add(new Word("kali"));
        expected.add(new Word("mans"));

        List<Word> actual = wordManager.excludeWordsContainingChars(words, excludedChars);

        assertEquals(expected.size(), actual.size());
        assertEquals(expected.get(0).getId(), actual.get(0).getId());
        assertEquals(expected.get(1).getId(), actual.get(1).getId());

    }
}