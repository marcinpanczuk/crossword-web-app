package org.bitbucket.marcinpanczuk.crossword.services;

import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration("classpath:crossword-servlet-test.xml")
@ExtendWith(SpringExtension.class)
class WordManagerIT {

    @Autowired
    WordManager wordManager;

    @Test
    @Transactional
    @Rollback
    void should_GetWordFromDB_When_AddedToDB() {
        wordManager.addWord("źdźbło");
        Word actual = wordManager.getWord("źdźbło");
        Word expected = new Word("źdźbło");
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getLength(), actual.getLength());
    }

    @Test
    @Transactional
    @Rollback
    void should_ReturnFalse_When_AddedDuplicate() {
        boolean done = wordManager.addWord("źdźbło");
        boolean done1 = wordManager.addWord("źdźbło");
        assertTrue(done);
        assertFalse(done1);
    }

    @Test
    @Transactional
    @Rollback
    void should_ReturnNull_When_NoWordInDB() {
        Word actualNull = wordManager.getWord("abcd");
        assertNull(actualNull);
    }

    @Test
    @Transactional
    @Rollback
    void should_ReturnList_When_PatternMatch(){
        wordManager.addWord("źdźbło");
        List<Word> results = wordManager.getWordByPattern("%bł%");
        assertEquals("źdźbło", results.get(0).getId());
    }

}