package org.bitbucket.marcinpanczuk.crossword.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration("classpath:crossword-servlet-test.xml")
@ExtendWith(SpringExtension.class)
class DBManagerIT {

    @Autowired
    DBManager dbManager;

    @Test
    @Transactional
    @Rollback
    void should_ReturnTrue_When_InitDBWithOneFile() {
        boolean done = dbManager.initDB("test.txt");
        assertTrue(done);
    }

    @Test
    @Transactional
    @Rollback
    void should_ReturnTrue_When_InitDBWithMultipleFiles(){
        boolean done = dbManager.initDB("test.txt", "test2.txt");
        assertTrue(done);
    }

    @Test
    @Transactional
    @Rollback
    void should_ReturnFalse_When_FileDontExist(){
        boolean done = dbManager.initDB("tst.txt");
        assertFalse(done);
    }
}