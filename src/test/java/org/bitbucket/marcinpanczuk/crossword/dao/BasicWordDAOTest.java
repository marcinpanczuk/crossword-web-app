package org.bitbucket.marcinpanczuk.crossword.dao;

import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BasicWordDAOTest {

    @InjectMocks
    private WordDAO wordDAO;
    @Mock
    SessionFactory sessionFactory;
    @Mock
    Session session;
    @Mock
    Query query;

    @BeforeEach
    void setUp() {
        wordDAO = new BasicWordDAO();
        MockitoAnnotations.initMocks(this);
        Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
    }

    @Test
    void should_ReturnTrue_When_AddingWordSucceed() {
        Word word = new Word("źdźbło");
        Mockito.when(session.save(word)).thenReturn("źdźbło");

        boolean result = wordDAO.add(word);

        assertTrue(result);
    }

    @Test
    void should_ReturnFalse_When_AddingWordThrownException() {
        Word word = new Word("źdźbło");
        Mockito.when(session.save(word)).thenThrow(HibernateException.class);

        boolean result = wordDAO.add(word);

        assertFalse(result);
    }

    @Test
    void should_ReturnTrue_When_DeletingWordSucceed() {
        Word word = new Word("źdźbło");
        Mockito.doNothing().when(session).delete(word);

        boolean result = wordDAO.delete(word);

        assertTrue(result);
    }

    @Test
    void should_ReturnFalse_When_DeletingWordThrownException() {
        Word word = new Word("źdźbło");
        Mockito.doThrow(HibernateException.class).when(session).delete(word);

        boolean result = wordDAO.delete(word);

        assertFalse(result);
    }

    @Test
    void should_ReturnWord_When_ReceivedWordExist() {
        Word expected = new Word("źdźbło");
        Mockito.when(session.get(Word.class, "źdźbło")).thenReturn(new Word("źdźbło"));

        Word actual = wordDAO.get("źdźbło");

        assertEquals(expected, actual);
    }

    @Test
    void should_ReturnNull_When_ReceivedWordDontExist() {
        Mockito.when(session.get(Word.class, "źdźymbło")).thenReturn(null);

        Word word = wordDAO.get("źdźymbło");

        assertNull(word);
    }

    @Test
    void should_ReturnList_When_PatternMatch() {
        Word word1 = new Word("abccba");
        Word word2 = new Word("baccab");
        List<Word> expected = new ArrayList<>();
        expected.add(word1);
        expected.add(word2);
        String pattern = "__cc__";
        String hql = "from Word as w where w.id like '" + pattern + "'";
        Mockito.when(session.createQuery(hql)).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(expected);

        List<Word> actual = wordDAO.getByPattern(pattern);

        assertEquals(expected.size(), actual.size());
        assertSame(expected.get(0), actual.get(0));
        assertSame(expected.get(1), actual.get(1));
    }

    @Test
    void should_ReturnNull_When_CreateQueryThrowsException() {
        String pattern = "___bł_";
        String hql = "from Word as w where w.id like '" + pattern + "'";
        Mockito.when(session.createQuery(hql)).thenThrow(HibernateException.class);

        List<Word> actual = wordDAO.getByPattern(pattern);

        assertNull(actual);
    }
}