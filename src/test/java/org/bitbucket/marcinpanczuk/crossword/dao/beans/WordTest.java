package org.bitbucket.marcinpanczuk.crossword.dao.beans;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;


class WordTest {

    @ParameterizedTest
    @CsvSource(value = {"abcd,abcd",
                        "ABCD,abcd",
                        "ĄĆĘŁŃÓŚŹŻ,ąćęłńóśźż",
                        "  a bcd  ,a bcd",
                        "+-45$,+-45$"})
    void should_FormatString_When_SetId(String id, String expected) {
        Word word = new Word();

        word.setId(id);

        assertEquals(expected, word.getId());
    }

    @ParameterizedTest
    @CsvSource(value = {"abcd,4",
                        "abcdef,6",
                        "abc,3",})
    void should_SetLength_When_SetId(String id, int length){
        Word word = new Word();

        word.setId(id);

        assertEquals(length, word.getLength());
    }
}