package org.bitbucket.marcinpanczuk.crossword.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

@Service
public class DBManager {

    @Autowired
    private WordManager wordManager;

    public boolean initDB(String... files) {
        for (String file : files) {
            try {
                saveFileToDB(prepareFile(file));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private File prepareFile(String fileName) throws IOException {
        return new ClassPathResource(fileName).getFile();
    }

    private void saveFileToDB(File file) {
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] words = line.split(",");
                for (String word : words) {
                    wordManager.addWord(word);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
