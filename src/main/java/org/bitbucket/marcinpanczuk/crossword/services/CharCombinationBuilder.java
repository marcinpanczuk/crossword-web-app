package org.bitbucket.marcinpanczuk.crossword.services;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CharCombinationBuilder {

    public List<String> getCombinationsOfChars(String chars) {
        List<String> notUsedLetters = new ArrayList<>(Arrays.asList(chars.split("")));
        LettersHolder firstLettersHolder = new LettersHolder(notUsedLetters);
        Set<LettersHolder> holdersList = new HashSet<>();
        holdersList.add(firstLettersHolder);

        Set<LettersHolder> resultSet = new HashSet<>();
        while (0 != holdersList.iterator().next().getNotUsedLetters().size()) {
            holdersList = getNextIteration(holdersList);
            resultSet.addAll(getNextIteration(holdersList));
        }

        Set<String> combinationsSet = new HashSet<>();
        for (LettersHolder lettersHolder : resultSet) {
            combinationsSet.add(lettersHolder.usedLetters.get(0));
        }
        List<String> combinationsList = new ArrayList<>(combinationsSet);
        return combinationsList;
    }

    private Set<LettersHolder> combineUsedLettersWithEachNotUsed(LettersHolder lettersHolder) {
        Set<LettersHolder> results = new HashSet<>();
        for (int i = 0; i < lettersHolder.getNotUsedLetters().size(); i++) {
            String newCombination = lettersHolder.getUsedLetters().get(0) + lettersHolder.getNotUsedLetters().get(i);
            LettersHolder temp = new LettersHolder();
            temp.getUsedLetters().set(0, newCombination);
            temp.getNotUsedLetters().addAll(lettersHolder.getNotUsedLetters());
            temp.getNotUsedLetters().remove(i);
            results.add(temp);
        }
        return results;
    }

    private Set<LettersHolder> getNextIteration(Set<LettersHolder> lettersHolderSet) {
        Set<LettersHolder> results = new HashSet<>();
        for (LettersHolder lettersHolder : lettersHolderSet) {
            results.addAll(combineUsedLettersWithEachNotUsed(lettersHolder));
        }
        return results;
    }

    private class LettersHolder {
        private List<String> usedLetters = new ArrayList<>();
        private List<String> notUsedLetters = new ArrayList<>();

        public LettersHolder() {
            usedLetters.add("");
        }

        public LettersHolder(List<String> notUsedLetters) {
            this();
            this.notUsedLetters = notUsedLetters;
        }

        public List<String> getUsedLetters() {
            return usedLetters;
        }

        public List<String> getNotUsedLetters() {
            return notUsedLetters;
        }

        @Override
        public String toString() {
            return "LettersHolder{" +
                    "leftList=" + usedLetters +
                    ", rightList=" + notUsedLetters +
                    '}' + '\n';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LettersHolder that = (LettersHolder) o;
            return getUsedLetters().equals(that.getUsedLetters()) &&
                    getNotUsedLetters().equals(that.getNotUsedLetters());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getUsedLetters(), getNotUsedLetters());
        }
    }
}
