package org.bitbucket.marcinpanczuk.crossword.services;

import org.bitbucket.marcinpanczuk.crossword.dao.WordDAO;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.ScrabbleWord;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.Collator;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@Service
public class WordManager {

    @Autowired
    @Qualifier("basicWordDAO")
    private WordDAO wordDAO;

    public static String formatWord(String word) {
        return word.trim().toLowerCase();
    }

    @Transactional
    public boolean addWord(String word) {
        String formattedWord = formatWord(word);
        if ((wordDAO.get(formattedWord) != null)) {
            return true;
        } else {
            Word newWord = new Word(word);
            return wordDAO.add(newWord);
        }
    }

    @Transactional
    public Word getWord(String word) {
        return wordDAO.get(word);
    }

    @Transactional
    public List<Word> getWordByPattern(String pattern) {
        return wordDAO.getByPattern(pattern);
    }

    public List<Word> excludeWordsContainingChars(List<Word> words, String stringWithChars) {
        if (!stringWithChars.isEmpty()) {
            String[] chars = stringWithChars.split("");
            for (String excludedChar : chars) {
                Iterator iterator = words.iterator();
                while (iterator.hasNext()) {
                    Word word = (Word) iterator.next();
                    if (word.getId().contains(excludedChar)) {
                        iterator.remove();
                    }
                }
            }
        }
        return words;
    }

    public List<Word> sortWords(List<Word> words) {
        Locale locale = Locale.forLanguageTag("pl-PL");
        Collator collator = Collator.getInstance(locale);
        words.sort(Comparator.comparing(Word::getId, collator));
        return words;
    }

    public List<ScrabbleWord> sortByScore(List<ScrabbleWord> scrabbleWords){
          scrabbleWords.sort(Comparator.comparingInt(ScrabbleWord::getScore).reversed());
         return scrabbleWords;
    }
}
