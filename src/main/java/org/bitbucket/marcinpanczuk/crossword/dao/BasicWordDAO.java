package org.bitbucket.marcinpanczuk.crossword.dao;

import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BasicWordDAO implements WordDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean add(Word word) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(word);
            return true;
        } catch (HibernateException ex) {
            return false;
        }
    }

    @Override
    public boolean delete(Word word) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.delete(word);
            return true;
        } catch (HibernateException ex) {
            return false;
        }
    }

    @Override
    public Word get(String id) {
        Session session = sessionFactory.getCurrentSession();
        Word word = session.get(Word.class, id);
        return word;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Word> getByPattern(String pattern) {
        Session session = sessionFactory.getCurrentSession();
        try {
            String hql = "from Word as w where w.id like '" + pattern + "'";
            Query query = session.createQuery(hql);
            List<Word> results = query.getResultList();
            return results;
        } catch (HibernateException e) {
            return null;
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
