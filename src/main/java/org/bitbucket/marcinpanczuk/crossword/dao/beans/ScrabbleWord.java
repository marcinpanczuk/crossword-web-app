package org.bitbucket.marcinpanczuk.crossword.dao.beans;

import java.util.Comparator;

public class ScrabbleWord extends Word implements Comparable<ScrabbleWord>{

    private int score;
    public ScrabbleWord(){};

    public ScrabbleWord(String word){
        super(word);
        this.score = calculateScore(word);
    }

    public ScrabbleWord(Word word){
        super.setId(word.getId());
        this.score= calculateScore(word.getId());
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    private int calculateScore(String word){
        int score = 0;
        for(Character letter : word.toCharArray()){
            if (letter.equals('a') ||
                letter.equals('e') ||
                letter.equals('i') ||
                letter.equals('n') ||
                letter.equals('o') ||
                letter.equals('r') ||
                letter.equals('s') ||
                letter.equals('w') ||
                letter.equals('z')) {
                score+=1;
            } else if (letter.equals('c') ||
                    letter.equals('d') ||
                    letter.equals('k') ||
                    letter.equals('l') ||
                    letter.equals('m') ||
                    letter.equals('p') ||
                    letter.equals('t') ||
                    letter.equals('y')){
                score+=2;
            } else if (letter.equals('b') ||
                    letter.equals('g') ||
                    letter.equals('h') ||
                    letter.equals('j') ||
                    letter.equals('ł') ||
                    letter.equals('u')){
                score+=3;
            } else if (letter.equals('ą') ||
                    letter.equals('ę') ||
                    letter.equals('f') ||
                    letter.equals('ó') ||
                    letter.equals('ś') ||
                    letter.equals('ż')){
                score+=5;
            } else if (letter.equals('ć')){
                score+=6;
            } else if (letter.equals('ń')){
                score+=7;
            } else if (letter.equals('ź')){
                score+=9;
            }
        }
        return score;
    }

    @Override
    public int compareTo(ScrabbleWord o) {
        return Integer.compare(this.score, o.score);
    }
}
