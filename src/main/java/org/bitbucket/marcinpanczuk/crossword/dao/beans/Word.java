package org.bitbucket.marcinpanczuk.crossword.dao.beans;

import org.bitbucket.marcinpanczuk.crossword.services.WordManager;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "words")
public class Word {

    @Id
    @Column(name = "word", unique = true)
    private String id;

    @Column(name = "length")
    private int length;

    public Word() {
    }

    public Word(String id) {
        setId(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = WordManager.formatWord(id);
        setLength();
    }

    public int getLength() {
        return length;
    }

    private void setLength() {
        this.length = this.id.length();
    }

    @Override
    public String toString() {
        return "Word [ " + id + " , length: " + length + " ]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word = (Word) o;
        return getId().equals(word.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLength());
    }
}
