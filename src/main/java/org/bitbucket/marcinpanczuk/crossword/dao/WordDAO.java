package org.bitbucket.marcinpanczuk.crossword.dao;

import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;

import java.util.List;

public interface WordDAO {

    boolean add(Word word);

    boolean delete(Word word);

    Word get(String id);

    List<Word> getByPattern(String pattern);
}
