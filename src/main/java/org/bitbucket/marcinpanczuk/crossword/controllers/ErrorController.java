package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ErrorController {

    @RequestMapping("/error.htm")
    public ModelAndView responseToError(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Model model) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("errorPage");
        String errorMessage = "Unknown error";
        int errorCode = (Integer) httpServletRequest.getAttribute("javax.servlet.error.status_code");
        switch (errorCode){
            case 400: {
                errorMessage = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401 : {
                errorMessage="Http Error Code: 401. Unauthorized";
                break;
            }
            case 404 :{
                errorMessage = "Http Error Code: 404. Resource not found";
                break;
            }
            case 500 :{
                errorMessage = "Http Error Code: 500. Internal Server Error";
                break;
            }
        }
        mav.addObject("message", errorMessage);
        return mav;
    }
}
