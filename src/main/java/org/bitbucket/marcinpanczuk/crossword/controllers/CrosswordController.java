package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.bitbucket.marcinpanczuk.crossword.controllers.utils.CheckboxResultContainer;
import org.bitbucket.marcinpanczuk.crossword.controllers.utils.Pattern;
import org.bitbucket.marcinpanczuk.crossword.controllers.utils.PatternListContainer;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.bitbucket.marcinpanczuk.crossword.services.WordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CrosswordController {

    @Autowired
    private WordManager wordManager;

    @RequestMapping("/crossword.htm")
    public ModelAndView patternsLengthsForm() {
        ModelAndView mav = new ModelAndView();
        CheckboxResultContainer checkboxResult = new CheckboxResultContainer();
        mav.setViewName("crossword");
        mav.addObject("checkboxResult", checkboxResult);
        return mav;
    }

    @RequestMapping("/crosswordForm.htm")
    public ModelAndView patternsForm(@ModelAttribute("checkboxResult") CheckboxResultContainer result,
                                             BindingResult bindingResult) {
        List<Pattern> patterns = new ArrayList<>();
        for (String element : result.getList()) {
            Pattern pattern = new Pattern();
            pattern.setPatternLength(element);
            patterns.add(pattern);
        }
        PatternListContainer patternListContainer = new PatternListContainer();
        patternListContainer.setPatternList(patterns);
        ModelAndView mav = new ModelAndView();
        mav.setViewName("crosswordPatterns");
        mav.addObject("patternForm", patternListContainer);
        return mav;
    }

    @RequestMapping("/searchResult.htm")
    public ModelAndView searchResult(@ModelAttribute("patternForm") PatternListContainer patterns, BindingResult bindingResult) {
        List<List<Word>> summarizedResults = new ArrayList<>();
        for (Pattern pattern: patterns.getPatternList()){
            summarizedResults.add(wordManager.getWordByPattern(pattern.getPattern()));
        }
        ModelAndView mav = new ModelAndView();
        mav.setViewName("crosswordResult");
        mav.addObject("summarizedResults", summarizedResults);
        return mav;
    }

    @ModelAttribute("lengthsCheckboxList")
    public List<String> getLengthsCheckboxList() {
        List<String> list = new ArrayList<>();
        for (int i = 2; i <= 25; i++) {
            list.add(new String("" + i));
        }
        return list;
    }
}