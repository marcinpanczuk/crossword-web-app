package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.bitbucket.marcinpanczuk.crossword.dao.beans.ScrabbleWord;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.bitbucket.marcinpanczuk.crossword.services.CharCombinationBuilder;
import org.bitbucket.marcinpanczuk.crossword.services.WordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class ScrabbleController {

    @Autowired
    WordManager wordManager;
    @Autowired
    CharCombinationBuilder charCombinationBuilder;

    @RequestMapping("scrabble.htm")
    public ModelAndView scrabbleForm(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("scrabble");
        mav.addObject("characters", new String());
        return mav;
    }

    @RequestMapping("scrabbleForm.htm")
    public ModelAndView searchResults(@ModelAttribute("characters") String characters,
                                      BindingResult bindingResult){
        Set<ScrabbleWord> results = new HashSet<>();
        for (String combination : charCombinationBuilder.getCombinationsOfChars(characters)) {
            List<Word> wordsList = wordManager.getWordByPattern(combination);
            for(Word word : wordsList){
                results.add(new ScrabbleWord(word));
            }
        }
        ModelAndView mav = new ModelAndView();
        mav.setViewName("scrabbleResult");
        mav.addObject("results", wordManager.sortByScore(new ArrayList<>(results)));
        return mav;
    }
}
