package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.bitbucket.marcinpanczuk.crossword.controllers.utils.Pattern;
import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.bitbucket.marcinpanczuk.crossword.services.WordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class HangmanController {

    private String maxPatternLength = "25";
    @Autowired
    WordManager wordManager;

    @RequestMapping("/hangman.htm")
    public ModelAndView patternForm() {
        ModelAndView mav = new ModelAndView();
        Pattern pattern = new Pattern();
        String regex = "[A-Za-z_ąćęłńóśźż]+";
        pattern.setPatternLength(maxPatternLength);
        mav.setViewName("hangman");
        mav.addObject("pattern", pattern);
        mav.addObject("excludedChars", new String());
        mav.addObject("regex", regex);
        return mav;
    }

    @RequestMapping("/hangmanForm.htm")
    public ModelAndView searchResults(@ModelAttribute("pattern") Pattern pattern, BindingResult bindingResult,
                                      @ModelAttribute("excludedChars") String excludedChars, BindingResult bindingResult2) {
        List<Word> results = wordManager.getWordByPattern(pattern.getPattern());
        results = wordManager.excludeWordsContainingChars(results, excludedChars);
        ModelAndView mav = new ModelAndView();
        mav.setViewName("hangmanResult");
        mav.addObject("results", results);
        return mav;
    }
}
