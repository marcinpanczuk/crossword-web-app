package org.bitbucket.marcinpanczuk.crossword.controllers.utils;

import java.util.ArrayList;
import java.util.List;

public class PatternListContainer {
    private List<Pattern> patternList;

    public PatternListContainer() {
        this.patternList = new ArrayList<>();
    }

    public List<Pattern> getPatternList() {
        return patternList;
    }

    public void setPatternList(List<Pattern> patternList) {
        this.patternList = patternList;
    }
}