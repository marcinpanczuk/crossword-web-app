package org.bitbucket.marcinpanczuk.crossword.controllers.utils;

import java.util.ArrayList;
import java.util.List;

public class CheckboxResultContainer {
    private List<String> list;

    public CheckboxResultContainer() {
        this.list = new ArrayList<>();
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}