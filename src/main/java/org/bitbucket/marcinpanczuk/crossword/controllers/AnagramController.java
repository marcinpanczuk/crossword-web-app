package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.bitbucket.marcinpanczuk.crossword.dao.beans.Word;
import org.bitbucket.marcinpanczuk.crossword.services.CharCombinationBuilder;
import org.bitbucket.marcinpanczuk.crossword.services.WordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AnagramController {

    @Autowired
    WordManager wordManager;
    @Autowired
    CharCombinationBuilder charCombinationBuilder;

    @RequestMapping("anagram.htm")
    public ModelAndView anagramForm() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("anagram");
        mav.addObject("characters", new String());
        return mav;
    }

    @RequestMapping("anagramForm.htm")
    public ModelAndView searchResults(@ModelAttribute("characters") String characters,
                                      BindingResult bindingResult) {
        List<String> possibleCombinations = charCombinationBuilder.getCombinationsOfChars(characters);
        List<Word> results = new ArrayList<>();
        for (String combination : possibleCombinations) {
            Word temp = wordManager.getWord(combination);
            if (temp != null) {
                results.add(temp);
            }
        }
        ModelAndView mav = new ModelAndView();
        mav.setViewName("anagramResult");
        mav.addObject("results", wordManager.sortWords(results));
        return mav;
    }
}
