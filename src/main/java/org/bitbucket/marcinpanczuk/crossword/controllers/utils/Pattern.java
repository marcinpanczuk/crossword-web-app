package org.bitbucket.marcinpanczuk.crossword.controllers.utils;

public class Pattern {
    private String pattern;
    private String patternLength;

    public Pattern() {
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPatternLength() {
        return patternLength;
    }

    public void setPatternLength(String patternLength) {
        this.patternLength = patternLength;
    }
}