package org.bitbucket.marcinpanczuk.crossword.controllers;

import org.bitbucket.marcinpanczuk.crossword.services.DBManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DBController {

    @Value("${blockInit}")
    private String blockInit;
    @Autowired
    private DBManager dbManager;

    @RequestMapping(value = "/initDB.htm")
    public ModelAndView initDB() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("initDB");
        String status;
        if (blockInit.equals("false")) {
            boolean done = dbManager.initDB("words.txt", "odm.txt");
            status = done ? "Sukces" : "Porażka";
        } else {
            status = "Odmowa dostępu";
        }
        modelAndView.addObject("status", status);
        return modelAndView;
    }
}
