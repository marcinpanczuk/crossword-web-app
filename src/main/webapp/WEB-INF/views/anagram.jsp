<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% request.setCharacterEncoding("UTF-8"); response.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Panoramiczna.pl Anagram</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/styles/base.css"/>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="container">
    <header>
        <div class="text-center">
            <a href="/crossword-webapp-1.0-SNAPSHOT"><img src="<c:url value="/resources/images/logo.png"/>" alt="Panoramiczna.pl" width="100%"></a>
        </div>
    </header>
    <div class="container">
        <nav class="navbar justify-content-center">
            <ul class="nav nav-justified">
                <li class="nav-item">
                        <a href="crossword.htm"><div class="nav-link nav-btn-start">Krzyżówki</div></a>
                </li>
                <li class="nav-item">
                        <a href="hangman.htm"><div class="nav-link nav-btn-next">Wisielec </div></a>
                </li>
                <li class="nav-item">
                        <a href="scrabble.htm"><div class="nav-link nav-btn-next">Scrabble</div></a>
                </li>
                <li class="nav-item">
                        <a href="anagram.htm"><div class="nav-link nav-btn-next active">Anagramy/Literaki</div></a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="container">
<form:form action="anagramForm.htm">
    <table>
        <tr>
            <td>Pattern:</td>
            <td><input name="characters" required="required" pattern="[A-Za-ząćęłńóśźż]+" minlength="2" maxlength="8" size="8"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Search"/>
            </td>
        </tr>
    </table>
</form:form>
</div>
<div class="container">
        <footer class="card-footer">Copyright Marcin Pańczuk</footer>
    </div>
</body>
</html>