<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% request.setCharacterEncoding("UTF-8"); response.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Panoramiczna.pl Krzyżówka</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/styles/base.css"/>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body class="container">
        <header>
            <div class="text-center">
                <a href="/crossword-webapp-1.0-SNAPSHOT"><img src="<c:url value="/resources/images/logo.png"/>" alt="Panoramiczna.pl" width="100%"></a>
            </div>
        </header>
        <div class="container">
            <nav class="navbar justify-content-center">
                <ul class="nav nav-justified">
                    <li class="nav-item">
                            <a href="crossword.htm"><div class="nav-link nav-btn-start active">Krzyżówki</div></a>
                    </li>
                    <li class="nav-item">
                            <a href="hangman.htm"><div class="nav-link nav-btn-next">Wisielec </div></a>
                    </li>
                    <li class="nav-item">
                            <a href="scrabble.htm"><div class="nav-link nav-btn-next">Scrabble</div></a>
                    </li>
                    <li class="nav-item">
                            <a href="anagram.htm"><div class="nav-link nav-btn-next">Anagramy/Literaki</div></a>
                    </li>
                </ul>
            </nav>
        </div>
        <form:form action="searchResult.htm" modelAttribute="patternForm">
            <table>
                <c:forEach items="${patternForm.patternList}" var="pattern" varStatus="status">
                <tr>
                    <td>
                        Pattern for ${pattern.patternLength}
                    </td>

                    <td><form:input path="patternList[${status.index}].pattern" required="required" pattern="[A-Za-z_ąćęłńóśźż]+" minlength="${pattern.patternLength}" maxlength="${pattern.patternLength}" size="${pattern.patternLength}"/> </td>
                </tr>
            </c:forEach>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Search"/>
                    </td>
                 </tr>
            </table>
        </form:form>
        <div class="container">
            <footer class="card-footer">Copyright Marcin Pańczuk</footer>
        </div>
    </body>
</html>
