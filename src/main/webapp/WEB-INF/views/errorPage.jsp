<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% request.setCharacterEncoding("UTF-8"); response.setCharacterEncoding("UTF-8");%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Panoramiczna.pl Error</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/styles/base.css"/>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="container">
    <header>
        <div class="text-center">
            <a href="/crossword-webapp-1.0-SNAPSHOT"><img src="<c:url value="/resources/images/logo.png"/>" alt="Panoramiczna.pl" width="100%"></a>
        </div>
    </header>

    <div class="container">
        <span style="font-size: x-large;">${message}</>
</div>
<div class="container">
        <footer class="card-footer">Copyright Marcin Pańczuk</footer>
    </div>
</body>
</html>